# OpenML dataset: UFC-257-Poirier-vs.-McGregor-II-Tweets

https://www.openml.org/d/43787

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
UFC 257: Poirier vs. McGregor 2 was a mixed martial arts event produced by the Ultimate Fighting Championship that took place on January 24, 2021 at the Etihad Arena on Yas Island, Abu Dhabi, United Arab Emirates.
source: https://en.wikipedia.org/wiki/UFC_257
Content
Tweets gathered from January 22 to 26 which has at least one of the hashtags below

UltimateFightingChampionship
UFC57
UFC
Poirier
Mcgregor
PoirierMcgrego
McgregorPoirier
ConorMcgregor
DustinPoirier
MMA
UFCFightIsland
FightIsland

Acknowledgements
Thanks to Tweepy, Twitter, Python, Jupyter, UFC, Kaggle for hosting this dataset, and Microsoft for Azure
Inspiration
This data can be used for sentiment analysis pertaining to the UFC257 event

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43787) of an [OpenML dataset](https://www.openml.org/d/43787). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43787/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43787/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43787/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

